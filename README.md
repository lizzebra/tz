# tz



## Техническое задание 3         
Программа, которая считывает из файла числа, а далее отдельными функциями ищет среди этих чисел минимальное число, максимальное число, считает их общую сумму и произведение. Дополнительно к программе прописаны тесты для проверки.


## Общая информация

- файл tz.py - содержит функции чтения данных из файла, сложния, умножения, поиска минимума и максимума
- файл test_tz.py - содержит тесты для функций
- файл __init__.py - инициализатор
- файл tp.txt - файл с числовыми данными
- файл requirements.txt - содержит информацию о необходимой версии pytest


## Используемые версии

-  Python 3.9.1
-  Pytest-6.2.5


## Статус тестов
[![pipeline status](https://gitlab.com/lizzebra/tz/badges/main/pipeline.svg)](https://gitlab.com/lizzebra/tz/-/commits/main)
