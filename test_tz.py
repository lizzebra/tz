import random
from profilehooks import timecall
from tests.tz import addition, multiplication, minimum, maximum, numbs


def test_addition():
    try:
        result = addition(numbs)
        for n in numbs:
            result -= n
        assert result == 0
    except TypeError:
        assert 1 == 1


def test_multiplication():
    try:
        result = multiplication(numbs)
        for n in numbs:
            result /= n
        assert int(result) == 1
    except TypeError:
        assert 1 == 1


def test_minimum():
    try:
        result = minimum(numbs)
        assert result == min(numbs)
    except TypeError:
        assert 1 == 1


def test_maximum():
    try:
        result = maximum(numbs)
        assert result == max(numbs)
    except TypeError:
        assert 1 == 1


@timecall
def test_time():

    def big_list():
        r_list = []
        for i in range(len(numbs), 10000):
            n = random.randint(1, 100)
            r_list.append(n)

        res_one = addition(r_list)
        res_two = multiplication(r_list)
        res_three = minimum(r_list)
        res_four = maximum(r_list)

        return res_one, res_two, res_three, res_four

    big_list()
    assert True


def test_bug():
    try:
        raise OverflowError
    except OverflowError:
        print('Too big number')
