def read_data(filename: str) -> list:
    data = []
    with open(filename, 'rt', encoding='utf-8') as f:
        for line in f:
            parts = line.split()
    for p in parts:
        for each in p:
            data.append(int(each))
    return data


def addition(data):
    result = 0
    for d in data:
        result += d
    return result


def multiplication(data):
    result = 1
    for d in data:
        result *= d
    return result


def minimum(data):
    min_num = data[0]
    for d in data:
        if d < min_num:
            min_num = d
    return min_num


def maximum(data):
    max_num = data[0]
    for d in data:
        if d > max_num:
            max_num = d
    return max_num


numbs = read_data('tp.txt')
